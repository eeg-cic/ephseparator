# EPHseparator

This program will separate a two-seconds-eph file into 2 one-second-eph file.  
You can change the sampling rate at variable fs = ....  

[+Example: fs = 1000 for EGI.+]

## [-Caution-]
[-Make sure that the eph file is exactly 2 seconds according to fs-]
